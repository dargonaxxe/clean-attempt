package com.dargonaxxe.digitaloceanclient.presentation.presenters

import com.dargonaxxe.digitaloceanclient.presentation.ui.SignInView

interface ISignInPresenter : BasePresenter<SignInView> {
    fun signInButtonPressed(login: String, password: String)
}