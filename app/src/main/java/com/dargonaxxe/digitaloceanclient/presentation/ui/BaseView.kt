package com.dargonaxxe.digitaloceanclient.presentation.ui

interface BaseView {
    /**
     * Shows the progress bar until @link{hideProgressBar} called
     * */
    fun showProgressBar()

    /**
     * Hides progress bar
     * */
    fun hideProgressBar()

    /**
     * Displays warning with given message.
     * */
    fun showWarning(message: String)
}