package com.dargonaxxe.digitaloceanclient.presentation.presenters

import com.dargonaxxe.digitaloceanclient.presentation.ui.BaseView

interface BasePresenter<T : BaseView> {
    val view: T
}