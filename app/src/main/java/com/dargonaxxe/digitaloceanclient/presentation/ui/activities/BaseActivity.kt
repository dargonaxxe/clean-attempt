package com.dargonaxxe.digitaloceanclient.presentation.ui.activities

import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity()