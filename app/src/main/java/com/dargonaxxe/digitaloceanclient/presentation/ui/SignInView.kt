package com.dargonaxxe.digitaloceanclient.presentation.ui

import com.dargonaxxe.digitaloceanclient.presentation.presenters.ISignInPresenter

interface SignInView : BaseView {
    val presenter: ISignInPresenter
}