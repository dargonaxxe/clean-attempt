package com.dargonaxxe.digitaloceanclient.presentation.ui.activities

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.dargonaxxe.digitaloceanclient.R
import com.dargonaxxe.digitaloceanclient.presentation.presenters.ISignInPresenter
import com.dargonaxxe.digitaloceanclient.presentation.presenters.implementations.SignInPresenter
import com.dargonaxxe.digitaloceanclient.presentation.ui.SignInView

class SignInActivity : BaseActivity(), SignInView {
    override val presenter: ISignInPresenter = SignInPresenter(this)
    lateinit var submitButton: Button
    lateinit var loginEditText: EditText
    lateinit var passwordEditText: EditText

    override fun showProgressBar() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgressBar() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showWarning(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        initUI()
    }

//    Private methods

    private fun initUI() {
        loginEditText = findViewById(R.id.sign_in_login_edit_text)
        passwordEditText = findViewById(R.id.sign_in_password_edit_text)
        submitButton = findViewById(R.id.sign_in_submit_button)
        submitButton.setOnClickListener { notifySignInButtonPressed() }
    }

    private fun notifySignInButtonPressed() {
        val login = loginEditText.text.toString()
        val password = passwordEditText.text.toString()
        presenter.signInButtonPressed(login, password)
    }
}
